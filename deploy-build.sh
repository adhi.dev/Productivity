#/bin/bash
#author: v.adhithyan
MACHINE_PATH="/home/local/ZOHOCORP/adhithyan-3282"
DIR=$1
LOCATION="/home/local/ZOHOCORP/adhithyan-3282/Desktop/$DIR"
TOMCAT="Sas/tomcat"
TOMCAT_BIN="$LOCATION/$TOMCAT/bin/"
TOMCAT_CONF="$LOCATION/$TOMCAT/conf"
WEBAPPS="$LOCATION/$TOMCAT/webapps"
WEBINF="$WEBAPPS/ROOT/WEB-INF"
CONF="$WEBINF/conf/"
REACH="$CONF/Reach"
SocialService="$CONF/SocialService/"
CLASSES="$WEBINF/classes"
CLONE_URL="" #repository url for cloning
SCRIPT_DIR="$MACHINE_PATH/build-files"

mkdir $LOCATION
cp -a  Sas $LOCATION
cd $LOCATION
hg clone $CLONE_URL
cd $SCRIPT_DIR

cp *.sh $TOMCAT_BIN
cp server.xml $TOMCAT_CONF
cd $WEBAPPS
rm grid.war
unzip ROOT.war -d ROOT
cd $SCRIPT_DIR

cp -r  classes/* $CLASSES
#mkdir adhi
cp -r conf/* $CONF
cp -r reach/* $REACH
cp -r ss/* $SocialService
cp -r security/* $WEBINF
sh reinit.sh

