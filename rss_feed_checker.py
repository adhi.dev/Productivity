#author v.adhithyan
#python -m smtpd -n -c DebuggingServer localhost:1025
import feedparser
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

fname = "date.txt"

def save_published_date(date):
	file = open(fname, "w")
	file.write(date)
	file.close()
	return

def get_last_updated_date():
	file = open(fname, "r")
	date = file.read()
	file.close()
	return date

def mailme(published, updated, title, link):
	me = "my email address"
	passwd = "my password"
	to = "receiver email address"
	frm = me
	
	body = ("Published: " + published + "<br>")
	body = body + "Updated: " + updated + "<br>"
	body = body + "Refer:" + link + "<br>"	
	body = MIMEText(body, 'html')

	msg = MIMEMultipart('alternative')
	msg['From'] = me
	msg['To'] = to
	msg['Subject'] = "Xing RSS alert: " + title
	msg.attach(body)
	
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
	server.login(me, passwd)
	server.sendmail(frm, [to], msg.as_string())
	server.quit()
	return

def feedmailer():
	changelog = feedparser.parse('https://dev.xing.com/docs/changelog.atom')
	
	for i in range(0, len(changelog.entries)):
		published = str(changelog.entries[i].published)
		last_change  = str(get_last_updated_date())

		if last_change in published:
			if (i > 0):
				save_published_date(str(changelog.entries[i-1].published))
			print "quitting"
			return
		else:
			title = str(changelog.entries[i].title)
			updated = str(changelog.entries[i].updated)
			link = str(changelog.entries[i].link)
			title = str(changelog.entries[i].title)
			content = str(changelog.entries[i].content)
	
			mailme(published, updated, title, link)
	return

feedmailer()
