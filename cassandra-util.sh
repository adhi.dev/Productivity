#author v.adhithyan
#mkdir output in the folder in which this file is going to reside
#shell script to run cassandra queries from a text file and output them to another file
for queryFile in $(ls *.txt);
do
	echo $queryFile
	../cassandra-cli < $queryFile >> output/$queryFile
done
