#!/bin/bash
#author: v.adhithyan
#install wmctrl, xdotool
#does not work in mac os

open_new_tab () {
	
	WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
	xdotool windowfocus $WID
	xdotool key ctrl+shift+t
	wmctrl -i -a $WID
	#go_to_home_dir
	sleep 0.5  #sleep for 1/2 second
}

set_terminal_title() {
	xdotool key ctrl+t
	execute_command $1
}

execute_command() {
	xdotool type $1
	xdotool key Return
	sleep 0.5  #sleep for 1/2 second
}

go_to_home_dir(){
	execute_command 'cd'
}

open_new_tab_in_home_directory(){
	open_new_tab
	go_to_home_dir
}

#the following function takes 2 parameters:
#param 1: title to be set in terminal tab
#param 2: command to be executed in that tab
open_tab_with_params(){
	open_new_tab_in_home_directory
	set_terminal_title $1
	execute_command $2
}
#list of titles for terminal tab
#command to open new tab, requires xdotool.. ubuntu users install using command sudo apt-get install xdotool"


#step 1: launch terminal
TERMINAL="gnome-terminal"
$TERMINAL


#cd to server directory and start the server"
DESKTOP='~/Desktop/'
BASE=$1
BUILD='Sas/tomcat/bin'
RUN_SERVER='sh run.sh'
open_new_tab_in_home_directory
set_terminal_title 'build'
xdotool type "cd ~/Desktop/$BASE/$BUILD"
xdotool key Return

#start mysql
MYSQL='mysql -u root'
open_new_tab_in_home_directory
set_terminal_title 'mysql'
xdotool type 'mysql -u root'
xdotool key Return

#start_cassandra
open_new_tab_in_home_directory
set_terminal_title "cassandra"
xdotool type 'cd cassandra/bin'
xdotool key Return
execute_command "./cassandra"


#logs dir
open_new_tab_in_home_directory
set_terminal_title "logs"


#jar dir
open_new_tab_in_home_directory
set_terminal_title "jar"
$BIN_DIR="zohosocial/source/bin"
xdotool type "cd ~/Desktop/$BASE/$BIN_DIR"
xdotool key Return

#redis-server
REDIS_SERVER="redis-server"
open_tab_with_params $REDIS_SERVER $REDIS_SERVER

#redis-client
REDIS_CLIENT="redis-cli"
open_tab_with_params $REDIS_CLIENT $REDIS_CLIENT

#eclipse
#open_new_tab_in_home_directory
#set_terminal_title "eclipse"
#xdotool type 'cd eclipse'
#xdotool key Return
#execute_command './eclipse'

#open chrome
open_new_tab_in_home_directory
set_terminal_title "chrome"
xdotool type 'google-chrome-stable --proxy-auto-detect'
xdotool key Return

open_new_tab_in_home_directory
set_terminal_title "eclipse"
xdotool type 'cd eclipse'
xdotool key Return
execute_command './eclipse'
exit
