#!/bin/bash
#! author: adhithyan.v
#works fine on both mac and ubuntu
clear

SHOW_DATABASES="show databases"
MYSQL="mysql -B -u root"
DROP_DB="drop database "
DB_LIST="db.txt"
NEW_DB="create database jbossdb"
CASSANDRA="/Users/adhithyan-3282/cassandra/bin"

#change cassandra path and check whether schema.txt exists in a new machine

echo "show databases" | mysql -B -u root | grep "db$" > db.txt

cat db.txt | while read LINE
do
	echo "*****************************************"	
	echo "dropping database $LINE"
	echo "drop database $LINE" | mysql -B -u root
	echo "successfully dropped $LINE .."
	echo "*****************************************"
done
echo "*****************************************"
echo "creating database jbossdb"
echo "create database jbossdb" | mysql -B -u root
echo "created jbossdb"
echo "*****************************************"

rm $DB_LIST

if test $1 -eq 1
then 
	$1 = 3
fi

if test $1 -eq 3
then
	cd $CASSANDRA
	./cassandra-cli < schema.txt
fi


